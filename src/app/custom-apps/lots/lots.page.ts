import { CoreService } from './core.service';
import { MyChannelsService } from '../../common/services/my-channels.service';
import { NavController } from '@ionic/angular';
import { FirebaseService } from 'src/app/custom-apps/lots/firebase.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-lots',
  templateUrl: './lots.page.html',
  styleUrls: ['./lots.page.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class LotsPage implements OnInit {
  phoneNumber: string;
  userName: string;
  showSplash: true;
  shopChannels:any=[];
  splashImg:'';
  isOrgFooterEnabled:false;
  orgFooterLogo:'';
  
  

  constructor(public firedb: FirebaseService,private navCntrl:NavController,public myChannelProvider:MyChannelsService,public core:CoreService) {


  }
  openMyChannels(){}
  openPaymentPage(){}
  openShares(){}
  openFavourite(){}
  openShops(){

  }
  openAboutMoyo(){}
  openUserFeedbackPage(){}

  
  ngOnInit() {
    console.log("helloo---")
    this.firedb.initializeApplication().then(_ => {
      this.firedb.initDb().then(() => {
        if (!this.firedb.auth.currentUser.photoURL) {
          this.logout();
        } else {
          // this.firedb.initUser();
          this.firedb.auth.onAuthStateChanged(user => {
            if (!user) {
              this.logout();
            }
          });
          this.phoneNumber = this.firedb.phone;
          this.userName = this.firedb.userName;
          if(this.firedb.auth.currentUser.photoURL){
            console.log("this.phoneNumber",this.phoneNumber);
            this.navCntrl.navigateForward('/lots/home');
          }
          
          // this.chatsProvider.userName = firedb.userName;
          // splashScreen.hide();
          // this.myChannelProvider.getLocallyStoredChannels().then(() => {
          //   // this.nav.setRoot(this.homePage).then(() => {
          //   //   this.core.selectedOptionOnHamburger = this.homePage;
          //   //   setTimeout(() => {
          //   //     this.registerForNotificationOpen();
          //   //   }, 0);
          //   //   // this.core.justDismisLoading();
          //   // });
          // }).catch(error => {
          //   console.error(error);
          //   // this.nav.setRoot(this.homePage).then(() => {
          //     // this.core.selectedOptionOnHamburger = this.homePage;
          //     // setTimeout(() => {
          //     //   // this.registerForNotificationOpen();
          //     // }, 0);
          //     // this.core.justDismisLoading();
          //   });
          // });
          // this.chatsProvider.getCountOfUnreadMessages();
          // this.platform.resume.subscribe(() => {
          //   this.menu.close();
          // });
        }
      }).catch(error => {
        console.log("last:",error);
        this.navCntrl.navigateForward('/lots/register');
        // this.nav.setRoot(this.loginPage).then(() => {
        //   setTimeout(function () {
        //     // splashScreen.hide();
        //   }, 300);
        //   // this.core.justDismisLoading();
        // });
      });
    })




  }
  openMenuOptions(ev){

  }
  openProfile(){}

  logout() {
    // this.menu.close();
    // this.menu.enable(false);
    // this.analitics.navigationManuClosedOn(this.pageTitle);
    console.log('logout clicked');
    let user = this.firedb.auth.currentUser;
    this.firedb.auth.signOut().then(success => {

      this.firedb.db.collection('Users').doc(this.firedb.phone).get().then(doc => {
        let data = doc.data();
        if (data.DeleteUserOnLogout) {
          user.delete().then(() => {
            //this.firedb.deleteUserFromFirebase().then(() => {
            // this.navctl.navigateRoot('/lots/register');
            // navigateForward('/lots/home');
          }).catch(() => {
            console.log("Couldn't delete user from firebase!");
            // this.nav.setRoot(this.loginPage);
          })
        } else {
          // this.nav.setRoot(this.loginPage);
        }
      })
      // this.nav.setRoot('RegisterPage');

    }).catch(error => {
      console.error(error);
      // this.nav.setRoot(this.loginPage);
    });

  }


}
