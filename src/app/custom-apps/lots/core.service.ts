// import {  AlertController } from 'ionic-angular';
import { AlertController } from '@ionic/angular';


import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CoreService {
  // private loading: Loading;
  private loader: HTMLIonLoadingElement;
  loaderLoading: boolean = false;
  selectedOptionOnHamburger='';
  constructor(
    // public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public platform: Platform,
  ) { }
  isMobileApp() {
    // return (!document.URL.startsWith('http') || document.URL.startsWith('http://localhost:8080'));
    return !document.URL.startsWith('http') || this.platform.is('ios');
  }

  async justShowInterNetNotAvailableAlert() {
    // return this.alertCtrl
    //   .create({
    //     message:
    //       'Internet connection lost. Please check your connection and try again',
    //     title: 'Moyo',
    //     buttons: ['Ok'],
    //   })
    //   .present();

    const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'Moyo',
      message: 'Internet connection lost. Please check your connection and try again',
      buttons: ['OK']
    });

    await alert.present();


  }


  // justShowLoading() {
  //   // if (this.loading) {
  //   //   this.loading.dismissAll();
  //   //   this.loading = null;
  //   // }
  //   if (!this.loading) {

  //       this.loaderLoading = true;
  //       this.loadingCtrl.create({

  //           showBackdrop: true
  //       }).then(load => {
  //           this.loader = load;
  //           load.present().then(() => { this.loaderLoading = false; });
  //       });







  //     // this.loading = this.loadingCtrl.create({  
  //     //   spinner: 'hide',
  //     //   content: '<img src="assets/spinner/spinner.svg">',
  //     //   cssClass: 'transparent',
  //     // });
  //     // return this.loading.present();
  //   }
  // }
  // justDismisLoading() {
  //   return new Promise((resolve, reject) => {
  //     setTimeout(() => {
  //       if (this.loading) {
  //         this.loading.dismissAll();
  //         this.loading = null;
  //         return resolve();
  //       }
  //     }, 100);
  //   })
  // }
  async justShowMessage(msg: string) {

     const alert = await this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'Moyo',
      message: msg,
      buttons: ['OK'],

    })
    await alert.present();



    //   return this.alertCtrl
    //     .create({
    //       message: msg,
    //       title: 'Moyo',
    //       buttons: ['Ok'],
    //       enableBackdropDismiss: false,
    //     })
    //     .present();
  }
}
