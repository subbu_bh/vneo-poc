import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LotsPage } from './lots.page';

describe('LotsPage', () => {
  let component: LotsPage;
  let fixture: ComponentFixture<LotsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LotsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
