import {HttpClient} from '@angular/common/http';
import {LoalDatabaseService} from './provider/loal-database.service';
import {Injectable} from '@angular/core';
import {Platform} from '@ionic/angular';
import {Firebase} from '@ionic-native/firebase';
import * as firebase from 'firebase';
import {AppCrypto} from 'src/providers/cripto';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs';
import {resolve} from 'url';
import {reject} from 'q';

@Injectable({
    providedIn: 'root'
})

export class FirebaseService {


    cript = new AppCrypto();
    dbKey: string;
    env: boolean = false;
    db: firebase.firestore.Firestore;
    userDetails: UserConfig;
    auth: firebase.auth.Auth;
    fireCore: firebase.app.App;
    firestorage: firebase.storage.Storage;
    verificationId: string = '';
    confirmationResult: any;
    phone: string = '';
    userId: string = '';
    userName: string = '';
    userTypeName: string = '';
    userTypeId: string = '';
    serverUrl: string;
    profileImage: string= "";
    appversion:'V1.57.90';

    constructor(
        public plt: Platform,
        public http: HttpClient,
        public localdb: LoalDatabaseService) {

    }

    updateFirestoreSettings() {
        // this.auditDb.settings({ timestampsInSnapshots: true });
        // this.chatDB.settings({ timestampsInSnapshots: true });
        this.db.settings({timestampsInSnapshots: true});

        // if(this.isMobileApp()){
        // this.auditDb.enablePersistence().then(() => {
        // }).catch(error => {
        // });
        // this.chatDB.enablePersistence().then(() => {
        // }).catch(error => {
        // });
        this.db.enablePersistence().then(() => {
        }).catch(error => {
        });

        // }
    }

    public getCookie(name) {
        var cookieArray = document.cookie.split('; ');
        var cookieValue = null;
        cookieArray.forEach((cookieElement, index) => {
            if (cookieElement.indexOf('=')) {
                var elementArray = cookieElement.split('=');
                if (name === elementArray[0]) {
                    cookieValue = elementArray[1];
                    return false;
                }
            }
        });
        return cookieValue;
    };


    getSetBuildVersionInfo = () => {
        return new Promise((resolve, reject) => {
            if (this.env) {
                this.cript.auths =
                    'AI49FKyVdYYxDd96Xkjp6d_KyVdYYvpRAI49R4DDjKyVdYYxDd9e';
                this.dbKey = 'consumer-prod';


            } else {
                this.cript.auths = 'KyVdYYAI49R4DDF6Xkjp6b_KyVdYYxDd9ROuF6Xkjp6c';

                this.dbKey = 'consumer-qa';

            }
            this.serverUrl = 'https://qa.ngagepower.com/xcite/';
            resolve();
        });
    }

    private fromOnline(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.http.get(
            //     this.serverUrl +
            //     "KyVdYYzP9h_dSHLO9KP?a=" + this.dbKey).map(resp => resp.json()).toPromise().then(resp => {
            //         if (resp) {
            //             this.initFireApps(resp, resolve, reject);
            //         } else {
            //             reject("db initialization failed");
            //         }
            //     }).catch(err => {
            //         console.log(err);
            //         reject("db initialization failed");
            //     });
            this.getdatabaseConfigs().subscribe(res => {
                if (res) {
                    console.log('database response::', res);
                    this.initFireApps(res, resolve, reject);
                }
            });
        });
    }

    getdatabaseConfigs(): Observable<any> {
        return this.http.get<any>(
            this.serverUrl +
            'KyVdYYzP9h_dSHLO9KP?a=' + this.dbKey);
    }

    requestOTP(phone: string): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.plt.is('android')) {
                (<any>window).FirebasePlugin.verifyPhoneNumber(phone, 60, id => {
                    this.verificationId = id.verificationId;
                    resolve(id.verificationId);
                }, error => {
                    console.log('error: ' + error);
                    reject({error: error});
                });
            } else if (this.plt.is('ios')) {
                (<any>window).FirebasePlugin.getVerificationID(phone, id => {
                    console.log('verificationID: ' + id);
                    this.verificationId = id;
                    resolve(id);
                }, error => {
                    console.log('error: ' + error.error);
                    reject({error: error.error});
                });
            } else {
                reject({error: 'Sorry, this platform is not supported'});
            }
        })

        //let requestUrl = this.requestOTPUrl + "?name=" + name + "&phone=" + phone + "&email=" + email;
    }

    initWithCustomToken(token: string) {
        console.log(token);
        return new Promise((resolve, reject) => {
            //   this.http.post(this.serverUrl + "getConsumerCustomAuthKey", {
            //       key: token
            //   }).toPromise().then(res => res.json()).then(data => {
            //       if (data.code == 200) {
            //           this.auth.signInWithCustomToken(data.token).then(userCre => {
            //               console.log("signed in with custom auth:", userCre.user.phoneNumber);
            //               resolve(userCre);
            //           }).catch(error => {
            //               reject(error);
            //           })
            //       }
            //   })
            this.getConsumerCustomAuthKey(token).subscribe(res => {
                if (res) {
                    console.log('databsee response::', res);
                    // console.log();
                    this.auth.signInWithCustomToken(res.token).then(userCre => {
                        console.log('signed in with custom auth:', userCre.user.phoneNumber);
                        resolve(userCre);
                    }).catch(error => {
                        reject(error);
                    })
                    // resolve(res)
                } else {
                    reject()
                }
            });
        })
    }

    getConsumerCustomAuthKey(token: string): Observable<any> {
        return this.http.post<any>(
            this.serverUrl +
            'getConsumerCustomAuthKey', {
                key: token
            });
    }


    requestOTPForBrowser(phone: string, appVerifier: firebase.auth.ApplicationVerifier): Promise<any> {
        return new Promise((resolve, reject) => {
            this.auth.signInWithPhoneNumber(phone, appVerifier).then(confirmationResult => {
                this.confirmationResult = confirmationResult;
                resolve(confirmationResult)
            }).catch(error => {
                console.log(error);
                reject({error: error.message});
                // if (error.code == "auth/invalid-phone-number") {
                //     reject({ error: "Please select valid country code for your phone number" });
                // } else {
                //     reject({ error: "Something went wrong! Please try again." });
                // }

            })
        });
    }

    initializeApplication(): Promise<any> {
        // this.initPromise.push(this.getAppVersion())
        return this.getSetBuildVersionInfo();

    }

    private initFireApps(resp: any, resolve: Function, reject: Function) {

        let fireapp = firebase.initializeApp(resp.masterDb);
        this.firestorage = fireapp.storage();
        // fireapp.firestore().enablePersistence()
        this.db = fireapp.firestore();
        this.fireCore = fireapp;
        this.auth = fireapp.auth();
        // this.firestorage = fireapp.storage();
        //let auditApp = firebase.initializeApp(resp.auditDb, this.KEYS.AUDIT_DB);
        //this.auditDb = auditApp.firestore();
        // let chatApp = firebase.initializeApp(resp.chatDb, this.KEYS.CHAT_DB);
        // this.chatDB = chatApp.firestore();

        //  providerApp.firestore().enablePersistence();

        this.updateFirestoreSettings();
        this.auth.onAuthStateChanged(user => {
            if (this.auth.currentUser) {
                this.phone = this.auth.currentUser.phoneNumber;
                this.userName = this.auth.currentUser.displayName;
                // this.addNewPublicChannel();

                resolve();
            } else {
                reject('User object is undefined')
            }
        });
        // if (this.isMobileApp()) {
        this.localdb.setDatabaseInfo(resp);
        // }
    }

    initDb() {
        return new Promise((resolve, reject) => {
            this.localdb.getDataBaseInfo().then(resp => {
                if (resp && (!navigator.onLine || this.isMobileApp())) {
                    this.initFireApps(resp, resolve, reject);
                } else {
                    this.fromOnline().then(() => {
                        resolve();
                    }).catch(err => {
                        reject(err)
                        console.log(err);
                    });
                }
            }).catch(error => {
                this.fromOnline().then(() => {
                    resolve();
                }).catch(err => {
                    reject(err)
                    console.log(err);
                })
            })
        })
    }

    isMobileApp() {
        // return (!document.URL.startsWith('http') || document.URL.startsWith('http://localhost:8080'));
        return !document.URL.startsWith('http') || this.plt.is('ios');
    }
}

export interface UserConfig {
    Name: string;
    Phone: string;
    Email: string;
    Role: string;
    logError: boolean;
    logEvents: boolean;
}


