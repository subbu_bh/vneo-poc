import { NavController, MenuController } from '@ionic/angular';
import { FirebaseService } from '../firebase.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  constructor(public firedb:FirebaseService,public navctl:NavController,public menuCtrl: MenuController,) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
  }
  logout() {
    // this.menu.close();
    // this.menu.enable(false);
    // this.analitics.navigationManuClosedOn(this.pageTitle);
    console.log('logout clicked');
    let user = this.firedb.auth.currentUser;
    this.firedb.auth.signOut().then(success => {

      this.firedb.db.collection('Users').doc(this.firedb.phone).get().then(doc => {
        let data = doc.data();
        console.log("userdata:",data);
        if (data.DeleteUserOnLogout) {
          user.delete().then(() => {
            //this.firedb.deleteUserFromFirebase().then(() => {
            this.navctl.navigateRoot('/lots/register');
            // navigateForward('/lots/home');
          }).catch(() => {
            console.log("Couldn't delete user from firebase!");
            // this.nav.setRoot(this.loginPage);
          })
        } else {
          this.navctl.navigateRoot('/lots/register');
        }
      })
      // this.nav.setRoot('RegisterPage');

    }).catch(error => {
      console.error(error);
      // this.nav.setRoot(this.loginPage);
    });

  }

}
