import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
@Injectable({
  providedIn: 'root'
})
export class LoalDatabaseService {

  constructor(
    public storage: Storage
  ) { }

  setDatabaseInfo(info: Object) {
    return this.storage.set('data_db', info);
  }
  getDataBaseInfo() {
    return this.storage.get('data_db');
  }
}
