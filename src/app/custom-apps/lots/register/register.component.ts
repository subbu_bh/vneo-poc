import {Component, OnInit, ViewChild} from '@angular/core';
import * as firebase from 'firebase';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {MenuController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CoreService} from 'src/app/custom-apps/lots/core.service';
import {FirebaseService} from 'src/app/custom-apps/lots/firebase.service';
import {NavController, Platform, PopoverController} from '@ionic/angular';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    @ViewChild('termsCheckBox', {static: false})
    checkBox: any;
    @ViewChild('mobileNumber', {static: false})
    mobile: any;
    showSubmit = true;
    showExlametory = false;
    mobileInputType: string = 'tel';
    verificationId: string = '';
    public recaptchaVerifier: firebase.auth.RecaptchaVerifier;

    countryCodes: countryCode[] = [];
    defaultCode: string;
    forceVerifyByWebOtp = false;

    constructor(
        protected navCtrl: NavController,
        public plt: Platform,
        // protected navParams: NavParams,
        protected formBuilder: FormBuilder,
        // protected alertCtrl: AlertController,
        protected fireDb: FirebaseService,
        // protected loadingCtrl: LoadingController,
        protected popCtrl: PopoverController,
        private core: CoreService,
        private menu: MenuController,
        // private iab: InAppBrowser,
        protected platform: Platform
    ) {
        menu.enable(true);
        this.registerForm = formBuilder.group({
            countryCode: ['', Validators.compose([])],
            // 'email': ['', Validators.compose([Validators.required, EmailValidator.isValid])],
            mobile_number: ['', Validators.compose([Validators.required])],
            // 'user_name': ['', Validators.compose([Validators.required, UserNameValidator.isValid])],
            terms_accept: [false, Validators.compose([Validators.required])],
        });
        this.registerForm.get('countryCode').setValue('+61');
    }

    ngOnInit() {
        console.log('inside oninit')
        // this.menu.enable(false);
        this.loadCountryCodes();
    }

    ionViewDidLoad() {
        console.log(document.URL);
        console.log('viewDid Load');
        this.menu.enable(false);
        this.loadCountryCodes();
    }

    ionViewDidEnter() {
        this.menu.enable(false);
    }

    termsClicked() {
        // if(this.fireDb.getCookie('fcmToken')){
        //     window.webkit.messageHandlers.toContainer.postMessage("OPENTC");
        // }
        // else{
        //   this.openLink();
        // }
    }

    loadCountryCodes() {
        // this.core.justShowLoading();
        this.fireDb.db
            .collection('CountryCodes')
            .get()
            .then(codes => {
                // this.core.justDismisLoading();
                codes.forEach(code => {
                    let cc: countryCode = {
                        country: code.get('Name'),
                        code: code.get('Code'),
                        countryCode: code.get('CountryCode'),
                        id: code.id,
                    };
                    this.countryCodes.push(cc);
                });
                this.countryCodes = this.countryCodes.sort(
                    (one, two) => (one.country < two.country ? -1 : 1)
                );
                if (!this.defaultCode) {
                    this.defaultCode = this.countryCodes[0].code;
                }
                // this.fireDb.getCountryCode().subscribe(data => {
                //   this.fireDb.userSecrets = data.json();
                //   let country = this.countryCodes.find(code => code.countryCode == data.json().country);
                //   if (country) {
                //     this.defaultCode = country.code;
                //   } else {
                //     this.defaultCode = this.countryCodes[0].code;
                //   }
                // }, error => {
                //   this.defaultCode = this.countryCodes[0].code;
                // });
                this.registerForm.get('countryCode').setValue(this.defaultCode);
            })
            .catch(error => {
                // this.core.justDismisLoading();
                console.error(error);
                this.core.justShowMessage(
                    'Something wrong! Please check your internet connection and try again.'
                );
            });
    }

    validateMobile(formCtrl): string {
        if (formCtrl.get('mobile_number').hasError('required')) {
            return 'Please enter mobile number';
        }

        if (formCtrl.get('mobile_number').hasError('isNotValid')) {
            return 'Please enter valid mobile number';
        }
        return '';
    }

    requestForWeb(phone) {
        this.forceVerifyByWebOtp = true;
        this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
            'recaptcha-container'
        );
        this.showSubmit = false;
        // formCtrl.disable();
        this.recaptchaVerifier
            .verify()
            .then(success => {
                console.log(success);
                // this.core.justShowLoading();
                // this.fireDb
                //   .requestOTPForBrowser(phone, this.recaptchaVerifier)
                //   .then(success => {
                //     this.core.justDismisLoading();
                //     // this.core.userName = name;
                //     this.navCtrl
                //       .push('OtpverifyPage', {
                //         phone: phone,
                //         id: success,
                //         force: this.forceVerifyByWebOtp,
                //       })
                //       .then(success => {
                //         this.resetAllViews();
                //       });
                //   })
                //   .catch(error => {
                //     this.core.justDismisLoading();
                //     console.log(error);
                //     this.core.justShowMessage(error.error);
                //   });
            })
            .catch(error => {
                console.log(error);
                this.core.justShowInterNetNotAvailableAlert();
                // this.core.justDismisLoading();
            });
    }

    resetAllViews() {
        this.showSubmit = true;
        if (this.recaptchaVerifier) {
            this.recaptchaVerifier.clear();
        }
    }

    disableCheckBox: boolean = false;

    onSubmit(formCtrl: FormGroup) {
        let message: string = this.isValidated(formCtrl);
        if (message != '') {
            this.core.justShowMessage(message);
            return;
        }

        if (this.validateMobile(formCtrl) != '') {
            return;
        }
        let phone =
            formCtrl.get('countryCode').value.toString() +
            formCtrl.get('mobile_number').value.toString();

        if (!navigator.onLine) {
            console.log('No internet!! Device is offline!!');
            this.core.justShowMessage(
                'Internet connection lost. Please check your connection and try again'
            );
            return;
        } else {
            // let continuetoOtp = () => {
            //   this.core.justDismisLoading();
            //   this.disableCheckBox = true;
            //   if (!this.core.isMobileApp()) {
            //     this.requestForWeb(phone);
            //   } else {
            //     this.core.justShowLoading();
            //     this.fireDb
            //       .requestOTP(phone)
            //       .then(data => {
            //         this.core.justDismisLoading();
            //         // this.navCtrl
            //         //   .push('OtpverifyPage', {
            //         //     phone: phone,
            //         //     id: data,
            //         //     force: this.forceVerifyByWebOtp,
            //         //   })

            //         //   .then(success => this.resetAllViews());
            //           this.navCtrl.navigateForward('/route');


            //       })
            //       .catch(error => {
            //         // this.core.justDismisLoading();
            //         console.log(error);
            //         this.requestForWeb(phone);
            //         // this.core.justShowMessage(error.error);
            //       });
            //   }
            // };
            // this.core.justShowLoading();
            this.fireDb.db
                .doc(`Users/${phone}`)
                .get()
                .then(data => {
                    if (
                        data.exists &&
                        data.get('firebase_uuid')
                    ) {
                        this.fireDb
                            .initWithCustomToken(data.get('firebase_uuid'))
                            .then(user => {
                                console.log('user::', user);
                                this.gotoHomePage();
                            })
                            .catch(error => {
                                console.log('error part');
                                console.error(error);
                                // continuetoOtp();
                            });
                    } else {
                        // continuetoOtp();
                    }
                })
                .catch(error => {
                    console.error(error);
                    // continuetoOtp();
                });
        }
    }


    gotoHomePage() {
        // this.core.justDismisLoading();
        console.log('this.fireDb.auth.currentUser::', this.fireDb.auth.currentUser);
        this.fireDb.phone = this.fireDb.auth.currentUser.phoneNumber;

        this.navCtrl.navigateForward('/lots/home');
    }

    isValidated(formCtrl: FormGroup): string {
        this.showExlametory = true;
        if (formCtrl.get('mobile_number').hasError('required')) {
            return 'Please enter mandatory fields';
        }
        if (isNaN(formCtrl.get('mobile_number').value)) {
            return 'Please enter valid mobile number';
        }
        if (formCtrl.get('mobile_number').value.length < 10) {
            return 'Please enter valid mobile number';
        }
        if (formCtrl.get('mobile_number').value.length > 14) {
            return 'Please enter valid mobile number';
        }

        if (formCtrl.get('countryCode').hasError('required')) {
            console.log('countryCode fields not entred');
            return 'Please select Country code';
        }

        console.log(formCtrl.get('terms_accept').value);
        if (formCtrl.get('terms_accept').value == false) {
            return 'Please view and accept the T&C\'s';
        }

        return '';
    }

}

export interface countryCode {
    country: string;
    countryCode: string;
    code: string;
    id: string;
}
