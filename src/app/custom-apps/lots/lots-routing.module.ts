import {OtyverifyComponent} from './otyverify/otyverify.component';
import {HomeComponent} from './home/home.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {LotsPage} from './lots.page';
import {RegisterComponent} from './register/register.component';

const routes: Routes = [

    {
        path: '',
        component:LotsPage,
        children: [
            {
                path: '',
                component: RegisterComponent
            },
            {
                path: 'home',
                component: HomeComponent
            },
            {
                path: 'otpverify',
                component: OtyverifyComponent
            }
        ],

    },


];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LotsPageRoutingModule {
}
