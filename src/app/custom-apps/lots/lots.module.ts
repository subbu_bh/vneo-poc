import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';
import {FormGroup, FormControl, ReactiveFormsModule} from '@angular/forms';

import {LotsPageRoutingModule} from './lots-routing.module';

import {LotsPage} from './lots.page';
import {RegisterComponent} from './register/register.component';
import {HomeComponent} from './home/home.component';
import {OtyverifyComponent} from './otyverify/otyverify.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        LotsPageRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [LotsPage, RegisterComponent, HomeComponent, OtyverifyComponent],
})
export class LotsPageModule {
}
