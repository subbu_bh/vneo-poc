import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {QuickPage} from './quick.page';
import {BookAppointmentComponent} from './book-appointment/book-appointment.component';
import {LoginComponent} from './login/login.component';

const routes: Routes = [
    {
        path: '',
        component: QuickPage,
        children: [
            {
                path: '',
                component: LoginComponent
            },
            {
                path: '123',
                component: BookAppointmentComponent
            }
        ]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class QuickPageRoutingModule {
}
