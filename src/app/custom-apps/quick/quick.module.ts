import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {QuickPageRoutingModule} from './quick-routing.module';
import {QuickPage} from './quick.page';
import {BookAppointmentComponent} from './book-appointment/book-appointment.component';
import {LoginModule} from '../../common/login/login.module';
import {LoginComponent} from './login/login.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        QuickPageRoutingModule,
        LoginModule
    ],
    declarations: [
        QuickPage,
        BookAppointmentComponent,
        LoginComponent
    ]
})
export class QuickPageModule {
}
