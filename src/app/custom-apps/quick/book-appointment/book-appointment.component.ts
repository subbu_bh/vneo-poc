import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-book-appointment',
    templateUrl: './book-appointment.component.html',
    styleUrls: ['./book-appointment.component.scss'],
})
export class BookAppointmentComponent implements OnInit {

    constructor() {
    }

    service = 'eye';

    ngOnInit() {
    }

    selectme(v) {
        this.service = v;
    }

    gotooptodtimepage() {
    }

    gotooptodoctpage() {
    }

}
