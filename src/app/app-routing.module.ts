import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: 'quick', pathMatch: 'full'},
    {
        path: 'lots',
        children: [
            {
                path: '',
                loadChildren: () => import('./custom-apps/lots/lots.module').then(m => m.LotsPageModule)
            },
        ],
    },
    {
        path: 'quick',
        children: [
            {
                path: '',
                loadChildren: () => import('./custom-apps/quick/quick.module').then(m => m.QuickPageModule),
            }
        ],
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
