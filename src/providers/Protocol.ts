import { AppCrypto } from "./cripto";

export class AppProtocol {
  proto = "https";
  end = "moyoapp.co/";
  //end = "us-central1-moyo-52130.cloudfunctions.net/";
  constructor() {
  }

  initp(isSecured) {
    return isSecured ? this.proto + "" : this.proto;
  }

  initEnd(cripto: AppCrypto) {
    this.initEndqw();
    return this.end;
  }

  initpr(isSecured) {
    this.initpu(this.initEndq());
    this.initpy(this.initEndw());
    return isSecured ? this.proto + "-" + this.end.charAt(5) + "-" + this.end.length : this.proto;
  }

  initEndq() {
    return this.end.indexOf("dq");
  }

  initpu(isSecured) {
    return isSecured ? this.proto + "-5978-887-777-887" : this.proto;
  }

  initEndw() {
    return this.end.length;
  }

  initpy(isSecured) {
    return isSecured ? this.proto + "-9664433-23233-23454-65438-55879-124-356547" : this.proto;
  }

  initEndv() {
    this.initpr(false);
    return this.end.indexOf("hhre");
  }

  initpw(isSecured) {
    this.initEnde();
    return isSecured ? this.proto + "-8793-7654-2385-945" : this.proto;
  }

  initEnde() {
    this.initEndv();
    return this.end.replace("ii", "fe");
  }

  initpyf(isSecured) {
    this.initpw(false);
    return isSecured ? this.proto + "-0-88-7-123-4327" : this.proto;
  }

  initEndad() {
    this.initp(true);
    return this.end.sub();
  }

  initpp(isSecured) {
    this.initEndgh();
    return isSecured ? this.proto + "-87-05-34-23" : this.proto;
  }

  initEndqw() {
    this.initEndad();
    this.initpyf(false);
    return this.end.split("-");
  }

  initpqw(isSecured) {
    this.initpp(true);
    return isSecured ? this.proto + "-12-34-86-56" : this.proto;
  }

  initEndgh() {
    this.initpqw(false);
    return this.end.concat("sdjuhkfvb");
  }
}
