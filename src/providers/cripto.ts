import {AppProtocol} from "./Protocol";
export class AppCrypto {
  private possibleC = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_";
  private possibleN = "0123456789";
  seed = "117-115-45-99-101-110-116-114-97-108-49-45-109-111-121-111-45-51-99-101-55-98-46-99-108-111-117-100-102-117-110-99-116-105-111-110-115-46-110-101-116"
  auths = "";
  proto = new AppProtocol();
  geta(seed) {
    try {
      this.seedEncSave(seed);
    }catch (err){
      console.error(err);
    }
    return this.auths.split('_')[0];
  }

  getu() {
    return this.auths.split('_')[1];
  }

  initUrl() {
    let sha256 =  this.initProtocol();
    return this.addSlash(sha256)+this.buildD();
  }

  buildD() {
    this.prepareProto();
    return this.proto.initEnd(this)+"KyVdYYzP9h_dSHLO9KP"
  }

  prepareProto(){
    this.proto.initEndqw();
  }

  addSlash(value) {
    return value+"://";
  }

  addColun(value) {
    return value.concat(":");
  }

  initProtocol() {
    return this.proto.initp(true);
  }

  private getC(index: number) {
    return this.possibleC.charAt(index);
  }

  private getN(index: number) {
    return this.possibleN.charAt(index);
  }

  private cutC(index: number) {

  }

  private putC(c: string) {

  }

  seedDcript(seed){
    let nums = seed.split("-");
    let code = "";
    for(let i=0;i<nums.length;i++){
      code= code.concat(String.fromCharCode(parseInt(nums[i])));
    }
    return code;
  }

  seedEncSave(seed){
    let codeStr = "";

    for(let i=0;i<seed.length;i++){
      if(i>0){
        codeStr = codeStr+"-"+this.getC(i);
      }
      codeStr = codeStr+""+seed.charCodeAt(i)+this.getN(3);
    }
    return codeStr;
  }


}
